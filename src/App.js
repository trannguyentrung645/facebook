import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query'
import './App.css';
import { Sidebar, Contacts } from './features';
import { MainLayout, SecondLayout } from './layouts';
import { Home } from './pages';

function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Routes>
          <Route path="/" element={<MainLayout children={ <><Sidebar /> <Home /> <Contacts /> </> } />} />
          <Route path="/auth/login" element={<SecondLayout children={{isRegister: false}} />} />
          <Route path="/auth/register" element={<SecondLayout children={{isRegister: true}} />} />
        </Routes>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
