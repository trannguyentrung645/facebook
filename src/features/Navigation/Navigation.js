
import { NavLink } from 'react-router-dom';
import { AiFillHome } from 'react-icons/ai';
import { FaUserFriends } from 'react-icons/fa';
import { BsFillPlayBtnFill, BsMessenger } from 'react-icons/bs';
import { IoNotifications } from 'react-icons/io5';
import './Navigation.scss';

function Navigation() {

    return ( 
        <div className="nav-bar">
            <div className="nav-item left">
                <NavLink className="logo" to="/"> Facebook </NavLink>
            </div>

            <div className="nav-item middle">
                <NavLink className="link-item" to="/"> <AiFillHome className /> </NavLink>
                <NavLink className="link-item" to="/friends"> <FaUserFriends/> </NavLink>
                <NavLink className="link-item" to="/watch"> <BsFillPlayBtnFill/> </NavLink>
                <IoNotifications />
                <BsMessenger />
            </div>

            <div className="nav-item right">
                <div className="search-bar">
                    <input className="search" type="text" placeholder="Search" />
                </div>
                <div className="profile">
                    <img className="avatar" src="https://www.cnet.com/a/img/resize/60030d00e4f8014cbcc5402e317cc8ed1e268b4c/2022/06/13/67acf7dd-20c4-4fb4-af40-cea85f9da6c3/fvi65wzuuaaonhf.jpg?auto=webp&fit=crop&height=675&width=1200" alt="avatar" />
                </div>
            </div>
        </div>
    );
}

export default Navigation;