import './Contacts.scss';
import { Message } from '../';
import { useState, useEffect, useRef } from 'react'
import { URL } from '../../common/constants/constant';
import { useSelector } from 'react-redux';
import { useQuery } from 'react-query';

function Contacts() {

    const getContactsInfo = async () => {
        return await fetch(`${URL.user}/contacts/${user.username}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(res => res.json())
        // .then(res => { 
        //     return res;
        // })
    }

    const {data, isLoading, refetch} = useQuery('getContactsInfo', getContactsInfo, {
        refetchOnWindowFocus: false,
        cacheTime: 1000 * 60 * 60 * 24,
    });

    const socket = useSelector(state => state.user.socket);
    const user = useSelector(state => state.user);
    const refresh = useRef();
    const [contacts, setContacts] = useState([]);
    const [partner, setPartner] = useState();

    useEffect(() => {
        if (data) {
            setContacts(data);
        }
    }, [data]);

    const popupMessage = ((item) => {
        setPartner(item);
    })

    useEffect(() => {
        socket.emit('sendOnlineList', user.contacts);
        refresh.current = setInterval(() => {
            socket.emit('sendOnlineList', user.contacts);
        }, 10000);
        return () => {
            console.log("CLEANUP");
            clearInterval(refresh.current);
        }
    }, [JSON.stringify(contacts)]);

    useEffect(() => {
        socket.on('receiveOnlineList', (data) => {
            setContacts((prev) => {
                prev.forEach((item) => {
                    const exist = data.find(contact => contact.username == item.username);
                    if (exist) {
                        item.online = true;
                    } else {
                        item.online = false;
                    }
                })
                return [...prev];
        })
    })}, [])

    useEffect(() => {
        socket.on('receiveMessage', (data) => {
            contacts.forEach(contact => {
                if (contact.username === data.from) {
                    popupMessage(data.from);
                }
            })
        })
    }, []);

    if (isLoading) {
        return <div>Loading...</div>
    }

    return ( 
        <div className="contacts">
            <h3>Contacts</h3>
            <ul className="contacts-list">
                {
                    contacts.map((item, index) => {
                        return (
                            <li className="contact-item" key={index} onClick={() => popupMessage(item)}>
                                {/* <img className="avatar" src={item.avatar} alt="avatar" /> */}
                                {item.online && <div className="status active"></div>}
                                {!item.online && <div className="status inactive"></div>}
                                <p>{item.name}</p>
                            </li>
                        )
                    })
                }
            </ul>

            {partner && <Message children={{partner: partner}} />}
        </div>
    );
}

export default Contacts;