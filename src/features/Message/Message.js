import './Message.scss';
import { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';

function Message( {children} ) {
    const partner = children.partner;
    const socket = useSelector(state => state.user.socket);
    const name = partner.username;
    const to = partner.socketId;
    const [messages, setMessages] = useState([]);
    const messageInputRef = useRef();
    useEffect(() => {
        socket.on('receiveMessage', (message) => {
            setMessages((prev) => {
                return [...prev, message];
            });
        });
        return () => {
            socket.on('disconnect', () => {
                socket.emit("disconnect");
            });
        }
    }, []);

    const sendMessage = (message) => {
        message.to = to;
        socket.emit('sendMessage', message);
        messageInputRef.current.value = '';
    }

    return ( 
        <div className="message">
            <h3>{name}</h3>
            <ul className="message-list">
                {
                    messages.map((message, index) => {
                        return (
                            <li className="message-item" key={index}>
                                <textarea className="message-content" >{message}</textarea>
                            </li>
                        )
                    })
                }
            </ul>
            <div className="message-input">
                <input ref={messageInputRef} type="text" placeholder="Nhập tin nhắn" />
                <button className="message-send" onClick={() => sendMessage({content: messageInputRef.current.value})}>Gửi</button>
            </div>
        </div>
     );
}

export default Message;