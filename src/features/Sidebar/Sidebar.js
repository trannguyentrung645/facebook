import { NavLink } from 'react-router-dom';
import { FaUserFriends } from 'react-icons/fa';
import { BsFillPlayBtnFill, BsMessenger } from 'react-icons/bs';

import './Sidebar.scss';

function Sidebar() {
    return ( 
        <div className="side-bar">
            <div className="information">
                <img className="avatar" src="https://www.cnet.com/a/img/resize/60030d00e4f8014cbcc5402e317cc8ed1e268b4c/2022/06/13/67acf7dd-20c4-4fb4-af40-cea85f9da6c3/fvi65wzuuaaonhf.jpg?auto=webp&fit=crop&height=675&width=1200" alt="avatar" />
                <p>Trần Nguyên Trung</p>
            </div>
            <NavLink className="link" to="/friends"> <FaUserFriends className="icon" /> Friends </NavLink>
            <NavLink className="link" to="/watch"> <BsFillPlayBtnFill className="icon" /> Watch </NavLink>
            <NavLink className="link" to="/messenger"> <BsMessenger className="icon" /> Messenger </NavLink>
            
        </div>
    );
}

export default Sidebar;