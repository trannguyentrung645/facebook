import { useQuery } from 'react-query';
import { useState, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';

import { Input, Button } from '@mui/material';
import { IoImage, IoSendSharp } from 'react-icons/io5';
import { HiOutlineEmojiHappy } from 'react-icons/hi';
import { RiLiveFill } from 'react-icons/ri';

import './Home.scss';
import { URL } from '../../common/constants/constant';
import { upload } from '@testing-library/user-event/dist/upload';


function Home() {
    const user = useSelector(state => state.user);
    const uploadRef = useRef([]);

    const [statusFile, setStatusFile] = useState([]);

    const uploadImage = (e) => {
        console.log(e);
    }

    const getPosts = async () => {
        return await fetch(URL.user + `/${user._id}`)                            
        .then(res => res.json())
        // .then(data => {
        //     return data;
        // })
    }

    const {data, isLoading, refetch} = useQuery('posts_api', getPosts, {
        refetchOnWindowFocus: false,
        cacheTime: 1000 * 60 * 60 * 24,
    });
    const [posts, setPosts] = useState([]);
    const postInputRef = useRef();

    const setPost = async (content) => {
        const post = {
            author: user._id,
            time: new Date(),
            content: {...content},
    
        }
        await fetch(URL.user + '/create-post', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(post)
        })
        postInputRef.current.value = '';
        refetch();
    }


    useEffect(() => {
        if (data) {
            console.log(data);
            setPosts(data);
        }
    }, [data]);

    if (isLoading) {
        return <div className="content">Loading...</div>
    }

    return ( 
        <div className="content">
            <div className="status">
                <div className="input-area">
                    <Input inputRef={postInputRef} onInput={(e) => uploadImage(e)} className="input" placeholder="What's on your mind ?" multiline disableUnderline={true} />
                    <button className="post" onClick={() => setPost({text: postInputRef.current.value, img: []})}><IoSendSharp className="icon post-btn" /></button>
                </div>
                
                <div className="status-actions">
                    <div className="status-action">
                        <RiLiveFill className="icon" />
                        <p>Live Stream</p>
                    </div>
                    <label htmlFor="contained-button-file">
                        <input ref={uploadRef} style={{ display: "none" }} id="contained-button-file" type="file" multiple />
                        <Button component="span" className="status-action">
                            <IoImage className="icon" /> 
                        </Button>
                    </label>
                    <div className="status-action">
                        <HiOutlineEmojiHappy className="icon" /> 
                        <p>Feeling/Acticity</p>
                    </div>
                </div>
            </div>
            <ul className="posts-list">
                {
                    posts.map((item, index) => {
                        return (
                            <li className="post-item" key={index}>
                                <div className="post-info">
                                    <img src={item.avatar} alt="avatar" className="avatar" />
                                    <div className="info">
                                        <p className="name">{item.name}</p>
                                        <p className="time">{item.time}</p>
                                    </div>
                                </div>
                                <div className="post-content">
                                    <p className="post-text">{item.content.text}</p>
                                    {item.content.img.length > 0 && <img src={item.content.img} alt="post-img" className="post-img" />}
                                </div>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
     );
}

export default Home;