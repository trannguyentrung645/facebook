
export const LOGGED_IN = "logged.in";


export const loggedIn = (payload) => {
    return {
        type: LOGGED_IN,
        payload: payload
    }
}