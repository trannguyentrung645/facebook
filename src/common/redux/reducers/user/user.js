import { LOGGED_IN } from "../../actions/user";

const userReducer = ( state = null, action ) => {
    switch (action.type) {
        case LOGGED_IN:
            return action.payload;
        default: 
            return null;
    }
}

export default userReducer;