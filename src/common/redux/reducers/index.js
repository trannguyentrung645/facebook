import userReducer from './user/user';
import { combineReducers } from 'redux';

const reducer = combineReducers({user: userReducer});
export default reducer;